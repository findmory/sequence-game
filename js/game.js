var activeColor = "transparent";


$("button").on("click", function(e) {
  console.log("click")
  var _color = e.target.id;
  activeColor = (_color == "clear") ? "transparent" : _color;
  $("#color").css("background-color", activeColor);
});


// setup board
var S = "\u{2660}";
var D = "\u{2666}";
var C = "\u{2663}";
var H = "\u{2665}";
var JOKER = "\u{1f0cf}";
var cards = {};
cards["cards0"] = [JOKER, "4"+C, "3"+C, "2"+C, "A"+C, "A"+S, "2"+S, "3"+S, "4"+S, JOKER];
cards["cards1"] = ["4"+H, "K"+C, "5"+C, "6"+C, "7"+C, "7"+S, "6"+S, "5"+S, "K"+S, "4"+D];
cards["cards2"] = ["3"+H, "5"+H, "Q"+C, "9"+C, "8"+C, "8"+S, "9"+S, "Q"+S, "5"+D, "3"+D];
cards["cards3"] = ["2"+H, "6"+H, "9"+H, "Q"+H, "T"+C, "T"+S, "Q"+D, "9"+D, "6"+D, "2"+D];
cards["cards4"] = ["A"+H, "7"+H, "8"+H, "T"+H, "K"+H, "K"+D, "T"+D, "8"+D, "7"+D, "A"+D];
cards["cards5"] = ["A"+D, "7"+D, "8"+D, "T"+D, "K"+D, "K"+H, "T"+H, "8"+H, "7"+H, "A"+H];
cards["cards6"] = ["2"+D, "6"+D, "9"+D, "Q"+D, "T"+S, "T"+C, "Q"+H, "9"+H, "6"+H, "2"+H];
cards["cards7"] = ["3"+D, "5"+D, "Q"+S, "9"+S, "8"+S, "8"+C, "9"+C, "Q"+C, "5"+H, "3"+H];
cards["cards8"] = ["4"+D, "K"+S, "5"+S, "6"+S, "7"+S, "7"+C, "6"+C, "5"+C, "K"+C, "4"+H];
cards["cards9"] = [JOKER, "4"+S, "3"+S, "2"+S, "A"+S, "A"+C, "2"+C, "3"+C, "4"+C, JOKER];

// render row 1
for (row = 0; row < 10; row++) {
  for (i = 0; i < 10; i++) {
    //get current card
    var _currCard = cards["cards" + row][i];

    // determine if it's red or black
    // append class="red" or class="black"
    // if it's club or spade back it "card black"  else "card red"
    if (_currCard.includes("\u{2663}") || _currCard.includes("\u{2660}")) {
      var colorClass = "card black";
    } else {
      var colorClass = "card red";
    }


    $("#row" + row).append('<div class="'+colorClass + '" id=' + i + '>' + _currCard + '</div>');
  }
}


$(".card").on("click", function(e) {
  var _card = e.target;
  $(_card).css("background-color", activeColor);
  activeColor = "transparent";
  $("#color").css("background-color", activeColor);
});
